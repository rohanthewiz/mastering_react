### Running the example
Point your browser to **public/index.html**

### Rebuilding bundle.js after changes to source javascripts (located in src/)
- `npm run build`

### Setting up a React ES6 Project from scratch using Node, Browserify and Babel
- `mkdir myproject; cd myproject`
- `npm init`
- If browserify is not installed (`which browserify`),
 install with `npm install -g browserify`
- `npm install --save react react-dom`
- Note: Babel >= 6 *no longer includes presets (transpilers) for es6 and jsx by default*
- `npm install --save-dev babelify`
- `npm install --save-dev babel-preset-es2015 babel-preset-react`
- In the project root, put the following in .babelrc
    `echo '{ "presets": ["es2015","react"] }' > .babelrc`

### Optionally, you can install the Babel cli
- `npm install --save-dev babel-cli`

#### Testing out Babel's JSX functionality with the Babel Cli

```
    # create a file to convert
    echo '<h1>Hello, world!</h1>' > index.js
    # transpile
    ./node_modules/.bin/babel index.js
```
