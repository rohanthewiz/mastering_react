import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
    render() {

        // Call Greeter component with a property
        return (
            <section id="app">
                <Greeter name="Bruce Wayne" />
                <Intro />
            </section>
        )
    }
}

class Greeter extends React.Component {
    render() {
        // Call Hello component with children passing the Greeter's name property
        return (
            <Header>
                Hello {this.props.name}
            </Header>
        )
    }
}

class Header extends React.Component {
    render() {
        return <h1>{this.props.children}</h1>
    }
}

class Intro extends React.Component {
    render() {
        return (
            <div className="intro">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nemo neque officia voluptates! Accusantium aperiam doloribus eaque fugit impedit, incidunt natus non obcaecati quos repellendus, soluta veritatis vitae voluptatum. Modi.
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('main_content'));
